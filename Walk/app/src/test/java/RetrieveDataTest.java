import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.lang.String;
import java.util.ArrayList;

import static junit.framework.Assert.*;
import static org.junit.Assert.assertEquals;

import inforgraphic.walk.RetrieveData;

public class RetrieveDataTest {

    RetrieveData data ;

    @Before
    public void setUp() throws InterruptedException {
        data = new RetrieveData();
        Thread.sleep(10000);
    }

    @Test
    public void testActivityExists() {
        assertNotNull(data);
    }


    @Test
    public void testArrays() {
        for(int i = 0; i < 4; i++){
            ArrayList<String[]> arrays = data.returnData(i);
            assertNotNull(arrays);
            for(int j = 0; j < arrays.size(); j++){
                String[] array = arrays.get(j);
                assertNotNull(array);
                for(int k = 0; k < array.length; k++){
                    String info = array[k];
                    assertNotNull(info);
                }
            }
        }
    }

    @Test
    public void testNamesandContinents() {
        String[] names = data.returnNames();
        int length = names.length;
        assertEquals(length, 144);
        String[] continents = data.returnContinents();
        length = continents.length;
        assertEquals(length, 7);
    }

}

