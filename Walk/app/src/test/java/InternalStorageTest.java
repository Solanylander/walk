
import org.junit.Test;
import static junit.framework.Assert.*;

import inforgraphic.walk.InternalStorage;

public class InternalStorageTest {

    /** cannot create as it is a private class and methods tested in MainActivityTest as
     * it requires a content and device to store the data in
     */
    @Test
    public void setTurnTest() {
        InternalStorage storage = new InternalStorage();
        assertNotNull(storage);
    }

}
