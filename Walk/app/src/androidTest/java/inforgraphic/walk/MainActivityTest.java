package inforgraphic.walk;


import android.app.Activity;
import android.app.Application;
import android.app.Instrumentation;
import android.opengl.Visibility;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ApplicationTestCase;
import android.test.TouchUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

import inforgraphic.walk.MainActivity;
import inforgraphic.walk.InternalStorage;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    MainActivity activity;
    PieChart chart;
    SeekBar seekBar;
    TextView Value;
    RadioButton button;

    public MainActivityTest() {
        super(MainActivity.class);
    }
    @Before
    protected void setUp() throws Exception {
        super.setUp();
        getInstrumentation().setInTouchMode(false);
        //setActivityInitialTouchMode(true);
        activity = getActivity();
        chart = activity.chart;
        activity.addData("","");
        activity.setSeekBar();
        seekBar = activity.seekBar;
        Value = activity.seekBarValue;
        InternalStorage.writeObject(activity,"test","testing");
        button = (RadioButton) activity.findViewById(R.id.radioButton);
        Log.i("tag", "SetUp");
    }

    @Test
    public void testActivityExists() {
        assertNotNull(activity);
        Log.i("tag", "testActivityExists");
    }

    @Test
    public void testChartExists() {
        assertNotNull(chart);
        Log.i("tag", "testChartExists");
    }

    @Test
    public void testSeekBar() throws InterruptedException {
        seekBar.setProgress(0);
        Thread.sleep(1000);
        for(int i = 1; i < 10; i++){
            Log.i("tag", String.valueOf(Value.getText()));
            assertEquals("Date: " + (1965 + (i * 5)), String.valueOf(Value.getText()));
            seekBar.setProgress(i);
            Thread.sleep(1000);
        }
    }

    @Test
    public void testStorage() throws IOException, ClassNotFoundException {
        String store = (String) InternalStorage.readObject(activity,"test");
        assertEquals(store, "testing");
    }

    @Test
    public void testRadio() {
        assertTrue(button.isChecked());
    }



}
