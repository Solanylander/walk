package inforgraphic.walk;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

public class MainActivity extends AppCompatActivity {
    private RetrieveData data;
    private Spinner continent;
    private String[] dates;
    private Spinner spinner;
    public SeekBar seekBar;
    public boolean pressed[];
    private ArrayList<Integer> clicked;
    private ArrayAdapter<CharSequence> adapter;
    private LinearLayout views;
    public TextView seekBarValue;
    private RadioGroup radioGroup;
    private String [] xData;
    private float[] yData;
    private TextView[] textViews;
    public PieChart chart;
    public RadioButton radio;

    /**
     * onCreate method. Creates the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinners();
        setSeekBar();
        radioButtons();
        setNames();
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        try {
            addition();
        }
        catch (Exception e) {
        }
        initialPull();

    }

    /**
     * sets the onChangeListener for the radioButtons
     */
    private void radioButtons() {
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    Log.i("tag", checkedId + "");
                    addition();
                } catch (Exception e) {
                }
            }
        });
        radio = (RadioButton) findViewById(R.id.radioButton);
        radio.performClick();
    }

    /**
     * sets the listeners for the continent filter and the data filter
     */
    public void spinners(){
        continent = (Spinner) findViewById(R.id.spinner);
        dates = new String[]{"2010","2005","2000","1995","1990","1985","1980","1975","1970"};
        clicked = new ArrayList<>();
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBarValue = (TextView) findViewById(R.id.textView);
        seekBar.incrementProgressBy(5);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                region();
                Log.i("tag","test1");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        views = (LinearLayout) findViewById(R.id.view);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.continents, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * When the application opens it checks if the data has been pulled. If not it attempts
     * to pull the data
     */
    public void initialPull(){
        try {
            InternalStorage.readObject(this, "pulled");
        }
        catch (Exception e) {
            pullData(null);
        }
    }

    /**
     * seekBarListener
     */
    public void setSeekBar(){
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarValue.setText("Date: " + (progress * 5 + 1970));
                if (clicked.size() < 2) {
                } else {
                    try {
                        addition();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar.setEnabled(false);
    }

    /**
     * retrieves information from server
     * @param view
     */
    public void pullData(View view){
       data = new RetrieveData();
        Toast.makeText(this, "Pulling Data", Toast.LENGTH_LONG).show();
        //forces the AI to wait until the previous animation is completed
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeNames();
                setNames();
            }
        }, 10000);
    }

    /**
     * deselects all countries
     * @param view
     */
    public void deselect(View view){
        for (int i = 0; i < pressed.length; i++) {
            if (pressed[i]) {
                textViews[i].performClick();
                textViews[i].setBackgroundColor(Color.parseColor("#f5eed1"));
                textViews[i].setTextColor(Color.BLACK);
            }
        }
    }



     /**
     * Stores data in cache
     */
    public void writeNames() {
        try {
            InternalStorage.writeObject(this, "names", data.returnNames());
            InternalStorage.writeObject(this, "continents", data.returnContinents());
            InternalStorage.writeObject(this, "2131558483", data.returnData(0));
            InternalStorage.writeObject(this, "2131558484", data.returnData(1));
            InternalStorage.writeObject(this, "2131558485", data.returnData(2));
            InternalStorage.writeObject(this, "2131558486", data.returnData(3));
            InternalStorage.writeObject(this, "pulled", "Yes");
            Toast.makeText(this, "Data Recieved", Toast.LENGTH_LONG).show();
            for (int i = 0; i < pressed.length; i++) {
                if (pressed[i]) {
                    textViews[i].performClick();
                    textViews[i].setBackgroundColor(Color.parseColor("#f5eed1"));
                    textViews[i].setTextColor(Color.BLACK);
                }
            }
            for(int i = 0; i < textViews.length; i++){
                textViews[i].setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Data Transfer Failed", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Sets the name list and adds an on click listener to each one that changes the
     * graph
     */
    public void setNames(){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        String[] names = new String[0];
        try {
            names = (String[]) InternalStorage.readObject(this, "names");
        }catch (Exception e){
        }
        pressed = new boolean[names.length];
        textViews = new TextView[names.length];
        for(int i = 0; i < names.length; i++) {
            pressed[i] = false;
            final TextView name = new TextView(this);
            textViews[i] = name;
            name.setText(names[i]);
            name.setLayoutParams(params);
            views.addView(name);
            final int j = i;
            name.setTextColor(Color.BLACK);
            name.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(!pressed[j]) {
                        pressed[j] = true;
                        name.setBackgroundColor(Color.parseColor("#d24444"));
                        name.setTextColor(Color.WHITE);
                        name.setTypeface(null, Typeface.BOLD);
                        clicked.add(j);
                    }else{
                        pressed[j] = false;
                        name.setBackgroundColor(Color.parseColor("#f5eed1"));
                        name.setTextColor(Color.BLACK);
                        name.setTypeface(null, Typeface.NORMAL);
                        for(int k = 0; k < clicked.size(); k++){
                           int q = clicked.get(k);
                            if(q == j){
                                clicked.remove(k);
                            }
                        }
                    }
                    try {
                        addition();
                    } catch (Exception e) {
                    }
                }
            });
        }
        region();
  }

    /**
     * Revises the graph data whenever any filter is changed
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void addition() throws IOException, ClassNotFoundException {
        int c = 0;
        int b = radioGroup.getCheckedRadioButtonId();
        String lev = b + "";
        String dat = seekBar.getProgress() * 5 + 1970 + "";
        for (int i = 0; i < dates.length; i++) {
            if (dat.equals(dates[i])) {
                c = i + 1;
            }
        }
        if(clicked.size() == 0) {
            xData = new String[2];
            yData = new float[2];
            xData[0] = "INSERT";
            xData[1] = "DATA";
            yData[0] = 50;
            yData[1] = 50;
            addData("","Insert Data");
        }
        else if (clicked.size() == 1) {
            xData = new String[9];
            yData = new float[9];
            ArrayList<String[]> add = (ArrayList<String[]>) InternalStorage.readObject(this, lev);
            String[] countryData = add.get(clicked.get(0));
            for (int k = 1; k < 10; k++) {
                xData[k-1] = String.valueOf(1965 + (k * 5));
                yData[k-1] = Float.parseFloat(countryData[k]);
                seekBar.setEnabled(false);
            }
            addData("Years",countryData[0]);
        }
        else{
            seekBar.setEnabled(true);
            xData = new String[clicked.size()];
            yData = new float[clicked.size()];
            ArrayList<String[]> add = (ArrayList<String[]>) InternalStorage.readObject(this, lev);
            for (int k = 0; k < clicked.size(); k++) {
                String[] countryData = add.get(clicked.get(k));
                xData[k] = countryData[0];
                yData[k] = Float.parseFloat(countryData[c]);
            }
            addData("Countries","");
        }
    }

    /**
     * filters the country list by region
     */
    public void region() {
        try {
            String shown = String.valueOf(continent.getSelectedItem());
            String[] names = (String[]) InternalStorage.readObject(this,"continents");
            String check;
            if (shown.equals("All")) {
                check = names[0] + names[1] + names[2] + names[3] + names[4] + names[5] + names[6];
            } else if (shown.contains("East Asia")) {
                check = names[0];
            } else if (shown.contains("Europe")) {
                check = names[1];
            } else if (shown.contains("Latin America")) {
                check = names[2];
            } else if (shown.contains("Middle East")) {
                check = names[3];
            } else if (shown.contains("North America")) {
                check = names[4];
            } else if (shown.contains("South Asia")) {
                check = names[5];
            } else {
                check = names[6];
            }
            Log.i("tag",check);
            for (int i = 0; i < textViews.length; i++) {
                if (check.contains(textViews[i].getText())) {
                    textViews[i].setVisibility(View.VISIBLE);
                } else {
                    textViews[i].setVisibility(View.GONE);
                }
            }
        }catch(Exception e){
        }
    }

    /**
     * Updates the data in the graph
     * @param tag
     */
    public void addData(String tag, String description) {
        chart = (PieChart)findViewById(R.id.chart);
        ArrayList<Entry> yVals=new ArrayList<>();
        for (int i=0;i<yData.length;i++) {
            yVals.add(new Entry(yData[i], i));
        }
        ArrayList<String> xVals=new ArrayList<>();
        for (int i=0;i<xData.length;i++) {
            xVals.add(xData[i]);
        }
        chart.setDrawHoleEnabled(true);
        chart.setHoleColorTransparent(true);
        chart.setHoleRadius(7);
        chart.setTransparentCircleRadius(10);
        PieDataSet dataSet=new PieDataSet(yVals,tag);
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);


        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);
        dataSet.setSliceSpace(0);
        dataSet.setSelectionShift(10);
        chart.setDescription(description);
        chart.animateXY(2000, 2000);
        PieData data=new PieData(xVals,dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        chart.setData(data);
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry entry, int i, Highlight highlight) {
                if (entry==null)
                    return;

                Toast.makeText(MainActivity.this, "in " + xData[entry.getXIndex()]+ " -> "+ entry.getVal() + "%", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });

    }
}

