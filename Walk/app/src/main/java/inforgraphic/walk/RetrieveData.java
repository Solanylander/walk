package inforgraphic.walk;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Retrieves data from the world back website
 * Created by solan_000 on 24/11/2015.
 */

public class RetrieveData {
    String[] level;
    private ArrayList<String[]> no = new ArrayList<String[]>();
    private ArrayList<String[]> prm = new ArrayList<String[]>();
    private ArrayList<String[]> sec = new ArrayList<String[]>();
    private ArrayList<String[]> ter = new ArrayList<String[]>();
    private String[] areas;

    public RetrieveData() {
        //Creates an array list with the education levels abbreviations
        level = new String[]{"NOED", "PRM.CMPT", "SEC.CMPT", "TER.CMPT"};
        //retrieves information from the 4 websites
            for (int j = 0; j < 4; j++) {
                String z = level[j];
                String address = "http://api.worldbank.org/countries/all/indicators/BAR." + z + ".15UP.ZS?per_page=15246";
                webConnect(address,j);
            }
        //retrieves the area codes and regions for all countries
        areaCodes();
    }



    /**
     * returns a String array with the information of one level of education
     * @param x
     * @return ArrayList<String[]>
     */
    public ArrayList<String[]> returnData(int x){
        if(x == 0) {
            return no;
        }else if(x == 1){
            return prm;
        }else if(x == 2){
            return sec;
        }else{
            return ter;
        }
    }

    /**
     * return the names of all the countries in a specific order
     * @return String[]
     */
    public String[] returnNames(){
        String[] countries = new String[no.size()];
        for (int i = 0; i < no.size(); i++){
            String[] country = no.get(i);
            countries[i] = country[0];
        }
        return countries;
    }

    /**
     * return region names
     * @return String[]
     */
    public String[] returnContinents(){
        return areas;
    }

    /**
     * Performs the HTTP pull request with all the region names and assigns them to the
     * correct string
     */
    private void areaCodes() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://api.worldbank.org/countries/all?per_page=264");
                    URLConnection yc = url.openConnection();
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    yc.getInputStream()));
                    String inputLine;
                    //stores the previous line with the countries name in
                    String name = "";
                    areas = new String[7];
                    while ((inputLine = in.readLine()) != null) {
                        //checks if the legion has information on the region
                        if (inputLine.contains("<wb:region id=")) {
                            String x = (String) name.subSequence(13, name.length() - 10);
                            if(inputLine.contains("EAP") || inputLine.contains("EAS")){
                                areas[0] += x;
                            }
                            if(inputLine.contains("ECA") || inputLine.contains("ECS")){
                                areas[1] += x;
                            }
                            if(inputLine.contains("LAC")  || inputLine.contains("LCN")){
                                areas[2] += x;
                            }
                            if(inputLine.contains("MNA")  || inputLine.contains("MEA")){
                                areas[3] += x;
                            }
                            if(inputLine.contains("NAC")){
                                areas[4] += x;
                            }
                            if(inputLine.contains("SAS")){
                                areas[5] += x;
                            }
                            if(inputLine.contains("SSA")  || inputLine.contains("SSF")){
                                areas[6] += x;
                            }
                        }
                        name = inputLine;
                    }
                    in.close();
                } catch (Exception e) {
                }
            }
        });
        thread.start();
    }

    /**
     * Performs the HTTP pull request from the web address x and assigns it to the correct ArrayList
     * using the int "tag"
     * @param x
     * @param level
     */
    private void webConnect(final String x, final int level) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(x);
                    URLConnection yc = url.openConnection();
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    yc.getInputStream()));
                    String inputLine;
                    int y = 0;
                    String[] z = new String[10];
                    String prevLine = "";
                    String name = "";
                    while ((inputLine = in.readLine()) != null) {
                        if (inputLine.contains("<wb:value>")) {
                            if(y % 10 == 0){
                                z[0] = (String) name.subSequence(24, name.length() - 13);
                                y++;
                            }
                            z[y % 10] = (String) inputLine.subSequence(14, inputLine.length() - 11);
                            if(y % 10 == 9) {
                                if(level == 0) {
                                    no.add(z);
                                }else if(level == 1){
                                    prm.add(z);
                                }else if(level == 2){
                                    sec.add(z);
                                }else{
                                    ter.add(z);
                                }
                                z = new String[10];
                            }
                            y++;
                        }
                        name = prevLine;
                        prevLine = inputLine;
                    }
                    in.close();
                } catch (Exception e) {
                }
            }
        });
        thread.start();
    }
}

